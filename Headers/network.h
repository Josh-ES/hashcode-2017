#ifndef NETWORK_DEF
    #define NETWORK_DEF

    #include <iostream>
    #include <fstream>
    #include <string>
    #include <sstream>
    #include <vector>
    #include "request.h"
    #include "cache.h"
    #include "endpoint.h"
    #include "video.h"
    #include "network.h"
    using namespace std;

    class Network {
    public:
        int nVideos;
        int nEndpoints;
        int nRequests;
        int nCaches;
        vector<Video> videos;
        vector<Endpoint> endpoints;
        vector<Request> requests;

        Network(string fileName);
        void readFile(string fileName);
        void readFirstLine(ifstream &stream);
        void readVideoSizes(ifstream &stream);
        void readEndpoints(ifstream &stream);
        Cache readCache(ifstream &stream);
        void readRequests(ifstream &stream);
    };

    Network::Network(string fileName) {
        readFile(fileName);
    }

    void Network::readFile(string fileName) {
        ifstream dataStream;
        dataStream.open("/Users/joshua/Projects/Apps/Google HashCode 2017/" + fileName + ".in");

        // read in parameters from first line
        readFirstLine(dataStream);

        // read in video sizes and create video objects
        readVideoSizes(dataStream);

        // read in and create end point objects
        readEndpoints(dataStream);

        // read in all requests
        readRequests(dataStream);
    }

    void Network::readFirstLine(ifstream &stream) {
        string line;
        getline(stream, line);
        istringstream iss(line);

        iss >> nVideos >> nEndpoints >> nRequests >> nCaches >> Cache::capacity;
    }

    void Network::readVideoSizes(ifstream &stream) {
        string line;
        getline(stream, line);
        istringstream iss(line);

        int id = 0;
        int size;

        while (iss >> size) {
            Video video(id, size);
            videos.push_back(video);
            id++;
        }
    }

    void Network::readEndpoints(ifstream &stream) {
        string line;

        for (int i = 0; i < nEndpoints; i++) {
            getline(stream, line);
            istringstream iss(line);
            Endpoint endpoint;
            int nCaches;

            // read the id and latency of the cache
            iss >> endpoint.latency >> nCaches;

            for (int i = 0; i < nCaches; i++) {
                Cache cache = readCache(stream);
                endpoint.caches.push_back(cache);
            }

            endpoints.push_back(endpoint);
        }
    }

    Cache Network::readCache(ifstream &stream) {
        string line;
        getline(stream, line);
        istringstream iss(line);
        Cache cache;

        // read in the cache's id number and latency
        iss >> cache.id >> cache.latency;

        return cache;
    }

    void Network::readRequests(ifstream &stream) {
        string line;

        while (getline(stream, line)) {
            istringstream iss(line);
            Request request;

            // read in the request's parameters
            iss >> request.videoId >> request.endpointId >> request.nRequests;

            requests.push_back(request);
        }
    }
#endif