#ifndef VIDEO_DEF
    #define VIDEO_DEF

    class Video {
    public:
        int id;
        int size;

        Video(int a, int b);
    };

    Video::Video(int a, int b) {
        id = a;
        size = b;
    }
#endif
