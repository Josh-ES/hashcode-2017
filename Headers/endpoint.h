#ifndef ENDPOINT_DEF
    #define ENDPOINT_DEF

    #include <vector>
    #include "cache.h"
    using namespace std;

    class Endpoint {
    public:
        int latency;
        vector<Cache> caches;
    };
#endif
