#include <vector>
#include "Headers/request.h"
#include "Headers/network.h"
using namespace std;

int main() {
    Network network("example");

    for(vector<Request>::iterator it = network.requests.begin(); it != network.requests.end(); ++it) {
        cout << it->nRequests << endl;
    }

    return 0;
}